unit SudoctorTable;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, ExtCtrls, GraphType, fgl,
  SudoctorDimensions, SudoctorElement, SudoctorCell,
  SudoctorCompletionSpace, SudoctorTablet;

type
  // A generic base for the tablet list (needed as an intermediate step)
  TSudoctorTabletListBase = specialize TFPGObjectList<TSudoctorTablet>;

  // A generic base for the compl. sp. list (needed as an intermediate step)
  TSudoctorCSpaceListBase = specialize TFPGObjectList<TSudoctorCompletionSpace>;

  { TSudoctorTabletList }

  TSudoctorTabletList = class(TSudoctorTabletListBase)
  private
    // The Items property getter
    function GetItems(Index: TSudoctorTabletIndex): TSudoctorTablet;
  public
    // Get the individual items
    property Items[Index: TSudoctorTabletIndex]: TSudoctorTablet
      read GetItems; default;
  end;

  { TSudoctorCSpaceList }

  TSudoctorCSpaceList = class(TSudoctorCSpaceListBase)
  private
    // The Items property getter
    function GetItems(Index: TSudoctorCSpaceIndex): TSudoctorCompletionSpace;
  public
    // Get the individual items
    property Items[Index: TSudoctorCSpaceIndex]: TSudoctorCompletionSpace
      read GetItems; default;
  end;

  { TSudoctorTable }

  TSudoctorTable = class(TPanel)
  private
    // The tablet list
    FTablets: TSudoctorTabletList;
    // The completion spaces
    FCompletionSpaces: TSudoctorCSpaceList;
    FTabletCompletionSpaces: TSudoctorCSpaceList;
    FRowCompletionSpaces: TSudoctorCSpaceList;
    FColCompletionSpaces: TSudoctorCSpaceList;
    // The entering mode property holder
    FEnteringMode: Boolean;
    // Plane tablet dimensions in cells
    function GetCellByCoords(X, Y: TSudoctorCellIndex): TSudoctorCell;
    // Collect the tablet completion spaces
    procedure CollectTabletCompletionSpaces;
    // Build a row completion space
    function BuildRowCompletionSpace(Row: TSudoctorCellIndex):
      TSudoctorCompletionSpace;
    // Build a column completion space
    function BuildColCompletionSpace(Col: TSudoctorCellIndex):
      TSudoctorCompletionSpace;
    // Build the row completion spaces
    procedure BuildRowCompletionSpaces;
    // Build the column completion spaces
    procedure BuildColCompletionSpaces;
    // Collect all the completion spaces
    procedure CollectAllCompletionSpaces;
    // The entering mode property setter
    procedure SetEnteringMode(AValue: Boolean);
    // Convert tablet coordinates to index
    function TabletCoordsToIndex(X, Y: Integer): TSudoctorTabletIndex;
    // Convert tablet index to coordinates
    function TabletIndexToCoords(Index: TSudoctorTabletIndex): TPoint;
    // Convert cell coordinates to tablet index
    function CellCoordsToTabletIndex(X, Y: TSudoctorCellIndex):
      TSudoctorTabletIndex;
    // The Tablets property getter
    function GetTablets(Index: TSudoctorTabletIndex): TSudoctorTablet;
    // The TabletByCoords property getter
    function GetTabletByCoords(X, Y: Integer): TSudoctorTablet;
    // The tablet change event handler
    procedure TabletChange(Sender: TObject);
  public
    // If the table is born
    constructor Create(TheOwner: TComponent); override;
    // If the table dies
    destructor Destroy; override;
    // New game
    procedure NewGame;
    // Get the individual child tablets
    property Tablets[Index: TSudoctorTabletIndex]: TSudoctorTablet
      read GetTablets;
    // Get a child tablet by its coordinates
    property TabletByCoords[X, Y: Integer]: TSudoctorTablet
      read GetTabletByCoords;
    // Get a cell by its coordinates
    property CellByCoords[X, Y: TSudoctorCellIndex]: TSudoctorCell
      read GetCellByCoords;
    // Are we in the entering phase?
    property EnteringMode: Boolean read FEnteringMode write SetEnteringMode;
  end;

const
  MinTabletIndex = Low(TSudoctorTabletIndex);
  MaxTabletIndex = High(TSudoctorTabletIndex);

implementation

{ TSudoctorCSpaceList }

// Get the individual completion spaces
function TSudoctorCSpaceList.GetItems(Index: TSudoctorCSpaceIndex
  ): TSudoctorCompletionSpace;
begin
  Result := inherited Get(Index);
end;

{ TSudoctorTabletList }

// Get the individual tablets
function TSudoctorTabletList.GetItems(Index: TSudoctorTabletIndex
  ): TSudoctorTablet;
begin
  Result := inherited Get(Index);
end;

{ TSudoctorTable }

// Get a cell by its coordinates
function TSudoctorTable.GetCellByCoords(X, Y: TSudoctorCellIndex): TSudoctorCell;
var
  TX, TY: Integer;      // the tablet coordinates
  CiTX, CiTY: Integer;  // the cell coordinates in the tablet
  Tablet: TSudoctorTablet;  // the target tablet
begin
  // Calculate the table coordinates
  TX:=X div TabletWidth;
  TY:=Y div TabletHeight;
  // Calculate the cell coordinates in the tablet
  CiTX:=X mod TabletWidth;
  CiTY:=Y mod TabletHeight;
  // Get the tablet
  Tablet := TabletByCoords[TX, TY];
  // Get the cell
  Result := Tablet.CellByCoords[CiTX, CiTY];
end;

{
  Convert tablet coordinates to tablet index.
  The coordinates are counted in tablets and are zero-based (index-like)
}
function TSudoctorTable.TabletCoordsToIndex(X, Y: Integer): TSudoctorTabletIndex;
begin
  Result := Y * HorzTabletCountInTable + X;
end;

function TSudoctorTable.TabletIndexToCoords(Index: TSudoctorTabletIndex
  ): TPoint;
begin
  Result.x:=Index mod HorzTabletCountInTable;
  Result.y:=Index mod VertTabletCountInTable;
end;

function TSudoctorTable.CellCoordsToTabletIndex(X, Y: TSudoctorCellIndex
  ): TSudoctorTabletIndex;
var
  TX, TY: Integer;
begin
  TX := X div TabletWidth;
  TY := Y div TabletHeight;
  Result := TabletCoordsToIndex(TX, TY);
end;

// Get the individual child tablets
function TSudoctorTable.GetTablets(Index: TSudoctorTabletIndex
  ): TSudoctorTablet;
begin
  Result := FTablets[Index];
end;

{
  Get a child tablet by its coordinates
  The coordinates are counted in tablets and are zero-based (index-like)
}
function TSudoctorTable.GetTabletByCoords(X, Y: Integer): TSudoctorTablet;
var
  TabletIndex: Integer;
begin
  TabletIndex:=TabletCoordsToIndex(X, Y);
  Result := FTablets[TabletIndex];
end;

// The tablet change event handler
procedure TSudoctorTable.TabletChange(Sender: TObject);
var
  I: Integer;                         // the compl. space index
  CSpace: TSudoctorCompletionSpace;   // the current completion space
begin
  // Go through all the completion spaces
  // and ublock their signs according to the change
  for I := 0 to Pred(FCompletionSpaces.Count) do
  begin
    CSpace := FCompletionSpaces[I];
    CSpace.UnblockSigns;
  end;
  // Go through all the completion spaces again
  // and block their signs again according to the change
  for I := 0 to Pred(FCompletionSpaces.Count) do
  begin
    CSpace := FCompletionSpaces[I];
    CSpace.BlockSigns;
  end;
end;

// If the table is born
constructor TSudoctorTable.Create(TheOwner: TComponent);
var
  X, Y: Integer;                                   // the coordinate-indices
  Tablet: TSudoctorTablet;                         // the current tablet
begin
  // Be born as a panel
  inherited Create(TheOwner);
  // Bevels
  BevelWidth:=1;
  BevelInner:=bvRaised;
  BevelOuter:=bvRaised;
  // Give birth to a tablet list
  FTablets := TSudoctorTabletList.Create;
  // Give birth to the tablets
  // Go through the rows
  for Y := 0 to MaxVertTabletIndex do
  begin
    // Go through the columns
    for X := 0 to MaxHorzTabletIndex do
    begin
      // This table is the tablet's owner
      Tablet := TSudoctorTablet.Create(Self);
      // This table is the tablet's parent
      Tablet.Parent := Self;
      // Add the tablet to the tablet list
      FTablets.Add(Tablet);
      // Set the position
      Tablet.Left:=X * Tablet.Width + BevelWidth * 2;
      Tablet.Top:=Y * Tablet.Height + BevelWidth * 2;
      // Set the change event handler
      Tablet.OnChange:=@TabletChange;
    end;
  end;
  // We shall adapt our size to the extent of the right-bottom-most tablet
  ClientWidth:=FTablets[MaxTabletIndex].BoundsRect.Right + BevelWidth * 2;
  ClientHeight:=FTablets[MaxTabletIndex].BoundsRect.Bottom + BevelWidth * 2;
  // Let's build the completion spaces
  // Collect the tablet completion spaces
  CollectTabletCompletionSpaces;
  // Build the row completion spaces
  BuildRowCompletionSpaces;
  // Build the column completion spaces
  BuildColCompletionSpaces;
  // Collect all the completion spaces
  CollectAllCompletionSpaces;
  // Enable entering mode
  EnteringMode:=True;
end;

{
  Collect the tablet completion spaces.
  Called at creation time.
}
procedure TSudoctorTable.CollectTabletCompletionSpaces;
var
  I: TSudoctorTabletIndex; // the index
  Tablet: TSudoctorTablet; // current tablet
begin
  // Create the completion space list
  FTabletCompletionSpaces := TSudoctorCSpaceList.Create(False);
  // Go through the tablets
  for I := MinTabletIndex to MaxTabletIndex do
  begin
    // Pick a tablet
    Tablet := Tablets[I];
    // Add its completion space to the list
    FTabletCompletionSpaces.Add(Tablet.Cells);
  end;
end;

{
  Build a row completion space.
  Called at creation time.
}
function TSudoctorTable.BuildRowCompletionSpace(Row: TSudoctorCellIndex
  ): TSudoctorCompletionSpace;
var
  Col: TSudoctorCellIndex; // the column index
  Cell: TSudoctorCell;     // the current cell
begin
  // Create a completion space
  Result := TSudoctorCompletionSpace.Create;
  // Go through the cells
  for Col:=MinCellIndex to MaxCellIndex do
  begin
    // Pick a cell
    Cell := CellByCoords[Col, Row];
    // Add the cell to the result
    Result.Add(Cell);
  end;
end;

{
  Build a column completion space.
  Called at creation time.
}
function TSudoctorTable.BuildColCompletionSpace(Col: TSudoctorCellIndex
  ): TSudoctorCompletionSpace;
var
  Row: TSudoctorCellIndex; // the row index
  Cell: TSudoctorCell;     // the current cell
begin
  // Create a completion space
  Result := TSudoctorCompletionSpace.Create;
  // Go through the cells
  for Row:=MinCellIndex to MaxCellIndex do
  begin
    // Pick a cell
    Cell := CellByCoords[Col, Row];
    // Add the cell to the result
    Result.Add(Cell);
  end;
end;

{
  Build all the row completion spaces.
  Called at creation time.
}
procedure TSudoctorTable.BuildRowCompletionSpaces;
var
  Y: TSudoctorCSpaceIndex;          // the row index
  CSpace: TSudoctorCompletionSpace; // the current completion space
begin
  // Create the completion space list
  FRowCompletionSpaces := TSudoctorCSpaceList.Create(True);
  // Go through the rows
  for Y := MinCellIndex to MaxCellIndex do
  begin
    // Build a row completion space
    CSpace := BuildRowCompletionSpace(Y);
    // Add it to the list
    FRowCompletionSpaces.Add(CSpace);
  end;
end;

{
  Build all the row completion spaces.
  Called at creation time.
}
procedure TSudoctorTable.BuildColCompletionSpaces;
var
  X: TSudoctorCSpaceIndex;          // the column index
  CSpace: TSudoctorCompletionSpace; // the current completion space
begin
  // Create the completion space list
  FColCompletionSpaces := TSudoctorCSpaceList.Create(True);
  // Go through the columns
  for X := MinCellIndex to MaxCellIndex do
  begin
    // Build a column completion space
    CSpace := BuildColCompletionSpace(X);
    // Add it to the list
    FColCompletionSpaces.Add(CSpace);
  end;
end;

procedure TSudoctorTable.CollectAllCompletionSpaces;
var
  T, C, R: TSudoctorCellIndex;
  TabCS, ColCS, RowCS: TSudoctorCompletionSpace;

begin
  FCompletionSpaces := TSudoctorCSpaceList.Create(False);
  // Add the tablet completion spaces
  for T := MinCellIndex to MaxCellIndex do
  begin
    TabCS := FTabletCompletionSpaces[T];
    FCompletionSpaces.Add(TabCS);
  end;
  // Add the column completion spaces
  for C := MinCellIndex to MaxCellIndex do
  begin
    ColCS := FColCompletionSpaces[C];
    FCompletionSpaces.Add(ColCS);
  end;
  // Add the row completion spaces
  for R := MinCellIndex to MaxCellIndex do
  begin
    RowCS := FRowCompletionSpaces[R];
    FCompletionSpaces.Add(RowCS);
  end;
end;

// The entering mode property setter
procedure TSudoctorTable.SetEnteringMode(AValue: Boolean);
var
  T: TSudoctorTabletIndex; // the tablet index
  Tablet: TSudoctorTablet; // the current tablet
begin
  if FEnteringMode=AValue then Exit;
  FEnteringMode:=AValue;
  // Go through all the tablets
  for T := MinTabletIndex to MaxTabletIndex do
  begin
    Tablet := Tablets[T];
    Tablet.EnteringMode:=FEnteringMode;
  end;
end;

// If the table dies
destructor TSudoctorTable.Destroy;
begin
  // Send the tablet list away
  FreeAndNil(FTablets);
  // Send the completion space lists away
  FreeAndNil(FCompletionSpaces);
  FreeAndNil(FTabletCompletionSpaces);
  FreeAndNil(FRowCompletionSpaces);
  FreeAndNil(FColCompletionSpaces);
  // Let the table die as a panel
  inherited Destroy;
end;

// Start over
procedure TSudoctorTable.NewGame;
var
  T: TSudoctorTabletIndex; // the index
  Tablet: TSudoctorTablet; // the current table
begin
  // Switch the entering mode on
  EnteringMode:=True;
  // Go through the tablets
  for T := MinTabletIndex to MaxTabletIndex do
  begin
    // Pick a tablet
    Tablet := FTablets[T];
    // Clear it
    Tablet.Clear;
  end;
end;

end.


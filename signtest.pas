unit SignTest;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls,
  sudoctorelement;

type

  { TSignTestForm }

  TSignTestForm = class(TForm)
    // A testing sudoku symbol control
    Symbol: TSudoctorElement;
    // State changing button (rotates the states of the symbol)
    btRotateState: TButton;
    // A label that shows the name of the current state of the symbol
    lbStateName: TLabel;
    tbEntering: TToggleBox;
    tbInputField: TToggleBox;
    // Happens when the form is born
    procedure FormCreate(Sender: TObject);
    // Happens if the symbol gets clicked
    procedure SymbolClick(Sender: TObject);
    // Happens if the rotate button gets clicked
    procedure btRotateStateClick(Sender: TObject);
    // Happens if the entering phase toggle box gets toggled
    procedure tbEnteringChange(Sender: TObject);
    // Happens if the input field toggle box gets toggled
    procedure tbInputFieldChange(Sender: TObject);
  private
    // Shows the current symbol state name
    procedure UpdateStateName;
  end;

var
  SignTestForm: TSignTestForm;

implementation

{$R *.lfm}

{ TSignTestForm }

// Happens when the main form is born ;-)
procedure TSignTestForm.FormCreate(Sender: TObject);
begin
  // Create a single sudoku symbol control for testing purposes.
  // We create it programatically for build flexibility
  {no need to recompile Lazarus}
  Symbol := TSudoctorElement.Create(Self);
  // This form will be its parent
  Symbol.Parent := Self;
  // Assign the click event handler to the symbol
  Symbol.OnClick:=@SymbolClick;
  // Set its digit to 1
  Symbol.SignNumber:=1;
  // Show the name of the current (default) state of the symbol control
  UpdateStateName;
end;

// Shows the current symbol state name
procedure TSignTestForm.UpdateStateName;
var
  SymbolState: TElementKind; // a local copy
  StateName: String;         // the corresponding name
begin
  // Grab the local copy of the current state of the symbol control
  SymbolState:=Symbol.Kind;
  // Pick the right name from the prepared symbol state names array
  StateName:=ElementKindNames[SymbolState];
  // Apply the name on the label's caption
  lbStateName.Caption:=StateName;
end;

// Happens if the symbol state changing button gets clicked
procedure TSignTestForm.btRotateStateClick(Sender: TObject);
var
  SymbolState: TElementKind; // a local copy
begin
  // Grab the local copy
  SymbolState:=Symbol.Kind;
  // Rotate the state
  if SymbolState < High(TElementKind) {below the highest value} then
    Inc(SymbolState) // grow it
  else // on the highest value
    SymbolState:=Low(TElementKind); // jump to the start
  // Apply the rotated state
  Symbol.Kind:=SymbolState;
  // Show the name of the new state
  UpdateStateName;
end;

// Happens if the entering phase toggle box gets toggled
procedure TSignTestForm.tbEnteringChange(Sender: TObject);
begin
  // Set or unset the entering phase state
  Symbol.EnteringMode:=tbEntering.Checked;
end;

// Happens if the input field toggle box gets toggled
procedure TSignTestForm.tbInputFieldChange(Sender: TObject);
begin
  // Take the control in an input field or out of it
  Symbol.InInputCell:=tbInputField.Checked;
end;

// Happens if the symbol gets clicked
procedure TSignTestForm.SymbolClick(Sender: TObject);
begin
  // Show the name of the current state of the symbol
  UpdateStateName;
end;

end.


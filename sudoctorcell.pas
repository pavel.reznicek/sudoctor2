unit SudoctorCell;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, ExtCtrls, GraphType, fgl,
  SudoctorElement, SudoctorDimensions;

type

  // The list of contained sign controls
  TSudoctorElementList = specialize TFPGList<TSudoctorElement>;

  { TSudoctorCell }
  { A cell with the choosable signs, typically nine }
  TSudoctorCell = class(TPanel)
  private
    FElements: TSudoctorElementList; // A list to hold all the elements
    // Property holders
    FIsInputCell: Boolean;
    FOnSignChange: TNotifyEvent;
    // The element click event handler
    procedure ElementClick(Sender: TObject);
    // The Elements property getter
    function GetElements(Number: TValidSignNumber): TSudoctorElement;
    function GetSignNumber: TSignNumber;
    procedure SetIsInputCell(AValue: Boolean);
    procedure SetSignNumber(AValue: TSignNumber);
  public
    // Happens if a new cell is born...
    constructor Create(TheOwner: TComponent); override;
    // Happens if the cell dies
    destructor Destroy; override;
    // Clear the cell
    procedure Clear;
    // Read-only property to get the individual elements
    property Elements[Number: TValidSignNumber]: TSudoctorElement
      read GetElements; default;
  published
    // A flag indicating that this cell is an input cell
    property IsInputCell: Boolean read FIsInputCell write SetIsInputCell;
    // A number representing the current sign chosen (0 ~ none)
    property SignNumber: TSignNumber read GetSignNumber write SetSignNumber;
    // The sign change event handler
    property OnSignChange: TNotifyEvent read FOnSignChange write FOnSignChange;
  end;

implementation

{ TSudoctorCell }

// Happend when an element changes its kind
procedure TSudoctorCell.ElementClick(Sender: TObject);
var
  Source: TSudoctorElement;  // the sender element converted to TSudoctorElement
begin
  // If we aren't an input cell
  if not FIsInputCell then
  begin
    // Convert the sender element to its native type
    Source := Sender as TSudoctorElement;
    // If an element is going to be chosen
    if Source.Kind = ekUnchosen then
      // Set the sign to the sign of the element that has just been clicked on
      SignNumber:=Source.SignNumber
    // If an element is going to be unchosen
    else if Source.Kind = ekChosen then
      SignNumber:=NoSignNumber;
  end;
end;

// The Elements property getter
function TSudoctorCell.GetElements(Number: TValidSignNumber): TSudoctorElement;
begin
  // Return the sign bearing the given number
  Result := FElements[Pred(Number)];
end;

// The sign number property setter
function TSudoctorCell.GetSignNumber: TSignNumber;
var
  I: TValidSignNumber;        // the Iterator
  Element: TSudoctorElement;  // the current element
begin
  // The result defaults to an empty cell value (0)
  Result := NoSignNumber;
  // Iterate through the elements and find the chosen one
  for I := MinSignNumber to MaxSignNumber do
  begin
    // Pick the element in question
    Element := Elements[I];
    // If it's chosen then return its number
    if Element.Kind = ekChosen then
    begin
      Result := I;
      Break;
    end;
  end;
end;

// The IsInputCell propery setter
procedure TSudoctorCell.SetIsInputCell(AValue: Boolean);
var
  E: TValidSignNumber;       // counter
  Element: TSudoctorElement; // current element
begin
  // If no change is needed then go away
  if FIsInputCell=AValue then Exit;
  // Set the property holder
  FIsInputCell:=AValue;
  // Inform the elements that we have become an input/solving cell
  for E:=MinSignNumber to MaxSignNumber do
  begin
    // Pick the current element
    Element := Elements[E];
    // Inform it
    Element.InInputCell:=AValue;
  end;
end;

// The sign number property setter
procedure TSudoctorCell.SetSignNumber(AValue: TSignNumber);
var
  OldValue: TSignNumber; // the old value
  I: TValidSignNumber;   // the Iterator
  Element, ElementToChoose, ElementToUnchoose: TSudoctorElement; // the elements

begin
  OldValue:=GetSignNumber; // save the old value
  // If a change is not needed then leave
  if AValue = OldValue then Exit;
  // If we are clearing the cell (sign number is 0)
  if AValue = 0 then
  begin
    // Pick the old chosen element
    ElementToUnchoose := Elements[OldValue];
    // Unchoose it
    ElementToUnchoose.Kind:=ekUnchosen;
  end
  // If we are going to choose an element
  else
  begin
    // Pick the element with the number given
    ElementToChoose := Elements[AValue];
    // Choose it
    ElementToChoose.Kind:=ekChosen;
    // Go through the rest of the elements
    for I:=MinSignNumber to MaxSignNumber do
    begin
      // Pick the target element
      Element := Elements[I];
      begin
        // If the target element sign is distinct from the sign being set
        if I <> AValue then
        begin
          // and if the target element is already chosen
          if Element.Kind = ekChosen then
          begin
            // unchoose it
            Element.Kind:=ekUnchosen;
          end;
        end;
      end;
    end;
  end;
  // Fire the event
  if Assigned(FOnSignChange) then
    FOnSignChange(Self);
end;

// If a new cell is born...
constructor TSudoctorCell.Create(TheOwner: TComponent);
var
  X, Y: Integer; // A sort of coordinate-indices
  Element: TSudoctorElement; // The current element we are adding
  HorzElemCount, VertElemCount: Integer; // Dimensions in Elements
  MaxHorzElemIndex, MaxVertElemIndex: Integer; // Highest coordinate-indices
  ElementSignNumber: TValidSignNumber; // The sign number of the current element
begin
  // Be born as a panel
  inherited Create(TheOwner);
  // Give birth to a element list
  FElements := TSudoctorElementList.Create;
  // Calculate the element count horizontally and vertically
  HorzElemCount := trunc(sqrt(MaxSignNumber));
  VertElemCount := HorzElemCount;
  // Calculate maximum element indices
  MaxHorzElemIndex := Pred(HorzElemCount);
  MaxVertElemIndex := Pred(VertElemCount);
  // Bevels
  BevelWidth:=1;
  BevelInner:=bvRaised;
  BevelOuter:=bvRaised;
  // Cycle the rows vertically
  for Y := 0 to MaxVertElemIndex do
  begin
    // Cycle the columns horizontally
    for X := 0 to MaxHorzElemIndex do
    begin
      // Create the Element; we are its owner
      Element := TSudoctorElement.Create(Self);
      // We are its parent
      Element.Parent := Self;
      // Add the Element to the Element list
      ElementSignNumber := Succ(FElements.Add(Element));
      // Move the Element to the right place
      Element.Left:=BevelWidth * 2 + X * Element.Width;
      Element.Top:=BevelWidth * 2 + Y * Element.Height;
      // Assign the right sign ;-)
      Element.SignNumber:=ElementSignNumber;
      // Assign the element click handler
      Element.OnClick:=@ElementClick;
    end;
  end;
  // We shall adapt our size to the extent of the right-bottom-most element
  ClientWidth:=Elements[1].Width * HorzElemCount + BevelWidth * 4;
  ClientHeight:=Elements[1].Height * VertElemCount + BevelWidth * 4;
end;

// If the cell dies...
destructor TSudoctorCell.Destroy;
begin
  // Burrow the sign list
  FreeAndNil(FElements);
  // Burrow the cell as a panel
  inherited Destroy;
end;

// Clear the cell
procedure TSudoctorCell.Clear;
var
  E: TValidSignNumber;       // the element number
  Element: TSudoctorElement; // the current element
begin
  // Go through the elements
  for E := MinSignNumber to MaxSignNumber do
  begin
    // Pick an element
    Element := Elements[E];
    // Free it
    Element.Kind:=ekUnchosen;
  end;
end;

end.


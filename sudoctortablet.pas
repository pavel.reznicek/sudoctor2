unit SudoctorTablet;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, ExtCtrls, GraphType, Dialogs, fgl,
  SudoctorCompletionSpace, SudoctorCell, SudoctorElement, SudoctorDimensions;

type

  { TSudoctorTablet }

  TSudoctorTablet = class(TPanel)
  private
    // Property holders
    FCells: TSudoctorCompletionSpace;
    FEnteringMode: Boolean;
    FOnChange: TNotifyEvent;
    procedure CellSignChange(Sender: TObject);
    // The CellByCoords property getter
    function GetCellByCoords(X, Y: Integer): TSudoctorCell;
    // The EnteringMode property setter
    procedure SetEnteringMode(AValue: Boolean);
  public
    // If a new tablet is born
    constructor Create(TheOwner: TComponent); override;
    // Convert cell coordinates to index
    function CellCoordsToIndex(X, Y: Integer): TSudoctorCellIndex;
    // Convert cell index to coordinates
    function CellIndexToCoords(Index: TSudoctorCellIndex): TPoint;
    // Clear the tablet
    procedure Clear;
    // Get individual cells
    property Cells: TSudoctorCompletionSpace read FCells;
    property CellByCoords[X, Y: Integer]: TSudoctorCell read GetCellByCoords;
    // Entering mode
    property EnteringMode: Boolean read FEnteringMode write SetEnteringMode;
    // Change event handler
    property OnChange: TNotifyEvent
      read FOnChange write FOnChange;
  end;


implementation

{ TSudoctorTablet }

procedure TSudoctorTablet.SetEnteringMode(AValue: Boolean);
var
  I: Integer;
  Cell: TSudoctorCell;
begin
  if FEnteringMode=AValue then Exit;
  FEnteringMode:=AValue;
  // Go through the cells and change them to input cells or vice versa
  for I := 0 to Pred(FCells.Count) do
  begin
    Cell := FCells[I];
    // If the cell has a sign
    if Cell.SignNumber > NoSignNumber then
    begin
      // set its input cell status according to the new mode
      if FEnteringMode then
        Cell.IsInputCell:=False
      else
        Cell.IsInputCell:=True;
    end
    // If the cell has no sign
    else
      // always turn its input cell status off
      Cell.IsInputCell:=False;
  end;
end;

// The cell sign change event handler
procedure TSudoctorTablet.CellSignChange(Sender: TObject);
begin
  // Inform the environment of the change
  if Assigned(FOnChange) then
    FOnChange(Self);
end;

{
  Get cells by plane coordinates.
  The coordinates are counted in cells and they are index-like (zero-based)
}
function TSudoctorTablet.GetCellByCoords(X, Y: Integer): TSudoctorCell;
var
  Index: TSudoctorCellIndex; // the resulting linear index
begin
  // Get the linear index
  Index:=CellCoordsToIndex(X, Y);
  // Get the cell
  Result := FCells[Index];
end;

constructor TSudoctorTablet.Create(TheOwner: TComponent);
var
  X, Y: Integer; // A sort of coordinate-indices
  Cell: TSudoctorCell; // The current cell that we are adding
begin
  // Let the tablet be born as a panel
  inherited Create(TheOwner);
  // Bevels
  BevelWidth:=1;
  BevelInner:=bvRaised;
  BevelOuter:=bvRaised;
  // Put the tablet in the entering mode
  FEnteringMode:=True;
  // Give birth to a cell list
  FCells := TSudoctorCompletionSpace.Create;
  // Populate the tablet with cells
  for Y:=0 to MaxVertCellInTabletIndex do
  begin
    for X:=0 to MaxHorzCellInTabletIndex do
    begin
      // Give birth to a cell
      Cell := TSudoctorCell.Create(Self);
      // This tablet will be its parent
      Cell.Parent := Self;
      // Add the cell to the party
      FCells.Add(Cell);
      // Set the position
      Cell.Left:=X * Cell.Width + BevelWidth * 2;
      Cell.Top:=Y * Cell.Height + BevelWidth * 2;
      // Set the sign change event handler
      Cell.OnSignChange:=@CellSignChange;
    end;
  end;
  // We shall adapt our size to the extent of the right-bottom-most cell
  ClientWidth:=FCells[MaxCellIndex].BoundsRect.Right + BevelWidth * 2;
  ClientHeight:=FCells[MaxCellIndex].BoundsRect.Bottom + BevelWidth * 2;
end;

{
  Convert cell coordinates to cell index.
  The coordinates are counted in cells and are zero-based (index-like)
}
function TSudoctorTablet.CellCoordsToIndex(X, Y: Integer): TSudoctorCellIndex;
begin
  Result := Y * TabletWidth + X;
end;

{
  Convert cell index to cell coordinates.
  The coordinates are counted in cells and are zero-based (index-like)
}
function TSudoctorTablet.CellIndexToCoords(Index: TSudoctorCellIndex): TPoint;
begin
  Result.X:=Index mod TabletWidth;
  Result.Y:=Index div TabletWidth;
end;

// Clear the tablet
procedure TSudoctorTablet.Clear;
var
  C: TSudoctorCellIndex; // the index
  Cell: TSudoctorCell;   // the current cell
begin
  for C := MinCellIndex to MaxCellIndex do
  begin
    Cell := FCells[C];
    Cell.Clear;
  end;
end;

end.


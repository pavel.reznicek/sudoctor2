unit SudoctorCompletionSpace;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fgl, SudoctorDimensions, SudoctorCell, SudoctorElement;


type
  // The list of contained sign controls
  TSudoctorCellList = specialize TFPGList<TSudoctorCell>;


  { TSudoctorCompletionSpace }

  TSudoctorCompletionSpace = class(TSudoctorCellList)
  private
    // The Cells property getter
    function GetCells(Index: TSudoctorCellIndex): TSudoctorCell;
  public
    {Unblocks the signs in the cells according to the chosen signs
    in the other cells}
    procedure UnblockSigns;
    {The opposite}
    procedure BlockSigns;
    // Tells if a sign is already chosen in another cell
    function SignAlreadyTaken(
      Sign: TValidSignNumber; ExcludedCellIndex: TSudoctorCellIndex
    ): Boolean;
    // Get the individual cells
    property Cells[Index: TSudoctorCellIndex]: TSudoctorCell
      read GetCells; default;
  end;

implementation

{ TSudoctorCompletionSpace }

// The Cells property getter
function TSudoctorCompletionSpace.GetCells(Index: TSudoctorCellIndex
  ): TSudoctorCell;
begin
  Result := Items[Index];
end;

{
  Unblocks the signs in the cells according to the chosen signs
  in the other cells
}
procedure TSudoctorCompletionSpace.UnblockSigns;
var
  I: TSudoctorCellIndex;                 // the target cell index
  J: TValidSignNumber;                   // the target element sign
  TargetCell: TSudoctorCell;             // the target cell
  TargetElement: TSudoctorElement;       // the target element
  TargetElementKind: TElementKind;       // the target element kind
  TargetIsInput: Boolean;                // Is the target cell an input cell?
  Taken: Boolean;                        // Is the sign already taken?
begin
  // Go through the target cells
  for I := MinCellIndex to MaxCellIndex do
  begin
    // Pick the target cell
    TargetCell := Cells[I];
    // Is the target cell an input cell?
    TargetIsInput:=TargetCell.IsInputCell;
    // If the target cell isn't an input cell
    if not TargetIsInput then
    begin
      // Go through the target elements
      for J := MinSignNumber to MaxSignNumber do
      begin
        // Pick the target element
        TargetElement := TargetCell.Elements[J];
        // Get its current kind
        TargetElementKind:=TargetElement.Kind;
        // If the target element is blocked
        if TargetElementKind = ekBlocked then
        begin
          // Is the target element sign already taken?
          Taken:=SignAlreadyTaken(J, I);
          // If not
          if not Taken then
            // ublock the element
            TargetElement.Kind:=ekUnchosen;
        end;
      end;
    end;
  end;
end;

{
  Blocks the signs in the cells according to the chosen signs
  in the other cells
}
procedure TSudoctorCompletionSpace.BlockSigns;
var
  I, J: TSudoctorCellIndex;              // the indices
  SourceCell, TargetCell: TSudoctorCell; // the cells
  SourceSign: TSignNumber;               // the source sign
  TargetElement: TSudoctorElement;       // the target element
  TargetIsInput: Boolean;                // Is the target cell an input cell?
begin
  // Go through source cells
  for I := MinCellIndex to MaxCellIndex do
  begin
    SourceCell := Cells[I];
    SourceSign:=SourceCell.SignNumber;
    // If a sign was chosen in the source cell
    if SourceSign > NoSignNumber then
    begin
      // Go through target cells
      for J := MinCellIndex to MaxCellIndex do
      begin
        if J = I then Continue;
        TargetCell := Cells[J];
        // Get the target element
        TargetElement := TargetCell[SourceSign];
        // Is the target cell an input cell?
        TargetIsInput:=TargetCell.IsInputCell;
        // If possible
        if not TargetIsInput then
          // block the element
          TargetElement.Kind:=ekBlocked;
      end;
    end;
  end;
end;

{
  Tells if the sign has been already taken in the completion space
  except in the given cell
}
function TSudoctorCompletionSpace.SignAlreadyTaken(
  Sign: TValidSignNumber; ExcludedCellIndex: TSudoctorCellIndex): Boolean;
var
  C: Integer;
  Cell: TSudoctorCell;
  CellSign: TSignNumber;
begin
  // Default result is false (= sign not found)
  Result := False;
  // Go through the cells
  for C := MinCellIndex to MaxCellIndex do
  begin
    // Skip the excluded cell
    if C = ExcludedCellIndex then Continue;
    // Pick the cell to query
    Cell := Cells[C];
    // Save its sign
    CellSign:=Cell.SignNumber;
    // If the cell sign is the sign given
    if CellSign = Sign then
    begin
      // we've just found it
      Result := True;
      Break;
    end;
  end;
end;

end.


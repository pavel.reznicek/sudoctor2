unit SudoctorElement;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, ExtCtrls, GraphType, Graphics, SudoctorDimensions;

type
  // We have four sign/digit states
  TElementKind = (ekUnchosen, ekChosen, ekBlocked);

  // Let's have an array type for holding the state names
  {used for debugging, see the SignStateNames constant}
  TElementKindNames = array[TElementKind] of String;

  // Color kind - background and foreground
  TColorKind = (ckForeground, ckBackground);

  // Color pair - foreground & background
  TElementColors = array[TColorKind] of TColor;

  // Maps the sign colors for all possible states
  TColorMap = array[Boolean, TElementKind] of TElementColors;

const
  // The actual array holding the sign state names
  {used for debugging}
  ElementKindNames: TElementKindNames = (
    'Unchosen', 'Chosen', 'Blocked'
  );

  clRose = $7F7FFF;

  // Maps the sign colors for all possible states
  ColorMap: TColorMap = (
    // Solving element
    (
      // solving, unchosen
      (clBlack, clLime),
      // solving, chosen
      (clBlack, clWhite),
      // solving, blocked
      (clOlive, clRose)
    ),
    // Input element
    (
      // input, unchosen
      (clDkGray, clBlack),
      // input, chosen
      (clWhite, clBlack),
      // input, blocked - technically the same as input, unchosen
      (clDkGray, clBlack)
    )
  );

type

  { TSudoctorElement }

  // The sudoku sign object. Holds digits for the time being.
  TSudoctorElement = class(TPanel)
  private
    // Data fields - property holders
    FSignNumber: TSignNumber;
    FKind: TElementKind;
    FEnteringMode: Boolean;
    FInInputCell: Boolean;
    FOnSignNumberChange: TNotifyEvent;
    FOnElementKindChange: TNotifyEvent;
    FOnEnteringModeChange: TNotifyEvent;
    FOnInInputCellChange: TNotifyEvent;
    // Setting the value (e. g., digit)
    procedure SetSignNumber(NewValue: TSignNumber);
    procedure UpdateCaption;      // apply the value
    // Setting the sign state
    procedure SetKind(NewValue: TElementKind);
    procedure SetEnteringMode(NewValue: Boolean);   // are we entering input?
    procedure SetInInputCell(NewValue: Boolean);  // are we in an input field?
    procedure UpdateColors;       // apply the right colors
    procedure UpdateDepression;   // apply the depressed/unpressed state
    procedure UpdateElementState;    // apply the sign state as a whole
  protected
  public
    // When a new sign is born. ;-)
    constructor Create(TheOwner: TComponent); override;
    // If the sign gets clicked
    procedure Click; override;
    // If the sign gets clicked twice
    procedure DblClick; override;
  published
    // The digit
    property SignNumber: TSignNumber
      read FSignNumber write SetSignNumber;
    // The sign kind
    property Kind: TElementKind read FKind write SetKind;
    // Are we entering input?
    property EnteringMode: Boolean read FEnteringMode write SetEnteringMode;
    // Are we in an input cell?
    property InInputCell: Boolean read FInInputCell write SetInInputCell;
    // The sign number change handler
    property OnSignNumberChange: TNotifyEvent
      read FOnSignNumberChange write FOnSignNumberChange;
    // The sign state change handler
    property OnElementKindChange: TNotifyEvent
      read FOnElementKindChange write FOnElementKindChange;
    // The entering mode change handler
    property OnEnteringModeChange: TNotifyEvent
      read FOnEnteringModeChange write FOnEnteringModeChange;
    // The being in an input cell change handler
    property OnInInputCellChange: TNotifyEvent
      read FOnInInputCellChange write FOnInInputCellChange;
  end;

const
  // Default sign size
  DefaultSignDimension = 20;

  // Default font properties
  {$IfDef MSWINDOWS}
  DefaultSignFontName = 'Courier New'; // well, I'm obsessed by monospace ;-)
  {$Else}
  DefaultSignFontName = 'Monospace';   // even if it goes to Linux ;-)
  {$EndIf}
  DefaultSignFontSize = 10;            // twelve points
  DefaultSignFontStyle = [fsBold];     // bold face

implementation

{ TSudoctorElement }

// The SignNumber property setter
procedure TSudoctorElement.SetSignNumber(NewValue: TSignNumber);
begin
  // If a change is required
  if NewValue <> FSignNumber then
  begin
    // change the value
    FSignNumber:=NewValue;
    // and write the right caption
    UpdateCaption;
    // If the event handler is set, raise it
    if Assigned(FOnSignNumberChange) then
      FOnSignNumberChange(Self);
  end;
end;

// Updates the sign's caption according to its numerical value
procedure TSudoctorElement.UpdateCaption;
begin
  // If we are in range (the high boundary is implied in TSignNumber)
  if FSignNumber >= Low(TValidSignNumber)
  then
    // display the digit,
    Self.Caption:=IntToStr(FSignNumber)
  else {should be 0}
    // hide the digit
    Self.Caption:='';
end;

// The SignState property setter
procedure TSudoctorElement.SetKind(NewValue: TElementKind);
begin
  // If a change is required
  if NewValue <> FKind then
  begin
    // change the value
    FKind:=NewValue;
    // show the new state
    UpdateElementState;
    // and if an event handler is assigned, call it
    if Assigned(FOnElementKindChange) then
      FOnElementKindChange(Self);
  end;
end;

procedure TSudoctorElement.SetEnteringMode(NewValue: Boolean);
begin
  // If a change id required
  if NewValue <> FEnteringMode then
  begin
    // change the value
    FEnteringMode:=NewValue;
    // show the new state
    UpdateElementState;
    // and if the event handler is assigned then call it
    if Assigned(FOnEnteringModeChange) then
      FOnEnteringModeChange(Self);
  end;
end;

procedure TSudoctorElement.SetInInputCell(NewValue: Boolean);
begin
  // If a change is required
  if NewValue <> FInInputCell then
  begin
    // change the value
    FInInputCell:=NewValue;
    // show the new state
    UpdateElementState;
    // and if the event handler is set then call it
    if Assigned(FOnInInputCellChange) then
      FOnInInputCellChange(Self);
  end;
end;

// Updates the sign state as a whole
procedure TSudoctorElement.UpdateElementState;
begin
  // Show the right depressed/unpressed state
  UpdateDepression;
  // Show the right colors
  UpdateColors;
end;

// Updates the sign's colors according to its state
procedure TSudoctorElement.UpdateColors;
begin
  // Pick the right colors from the prepared color map
  Self.Font.Color:=
    ColorMap[FInInputCell, FKind, ckForeground];
  Self.Color:=
    ColorMap[FInInputCell, FKind, ckBackground];
end;

// Updates the signs's "checked" (or "depressed") effect
// according to its state
{represented by the bevels here}
procedure TSudoctorElement.UpdateDepression;
begin
  case FKind of
    ekUnchosen, ekBlocked: // not depressed
      begin
        self.BevelInner:=bvRaised;
        Self.BevelOuter:=bvRaised;
      end;
    ekChosen: // depressed
      begin
        Self.BevelInner:=bvLowered;
        Self.BevelOuter:=bvLowered;
      end
    else
  end;
end;

constructor TSudoctorElement.Create(TheOwner: TComponent);
begin
  // Let the control initialize as a TPanel
  inherited Create(TheOwner);

  // Initialize the event handler
  FOnElementKindChange:=nil;

  // Set the size of the sign
  Self.Width:=DefaultSignDimension;
  Self.Height:=DefaultSignDimension;

  // Set the bevel width
  Self.BevelWidth:=1;

  // Set default font properties
  Self.Font.Name:=DefaultSignFontName;
  Self.Font.Size:=DefaultSignFontSize;
  Self.Font.Style:=DefaultSignFontStyle;

  // Initialize the digit to 0 (no digit)
  FSignNumber:=0;
  // and "show" it
  UpdateCaption;

  // Initialize the sign state to free
  FKind:=ekUnchosen;
  // and show it
  UpdateElementState;
end;

procedure TSudoctorElement.Click;
begin
  // Do what the click would do otherwise
  {e. g., fire events}
  inherited Click;
end;

procedure TSudoctorElement.DblClick;
begin
  // Do the same as on the click event
  Click;
  inherited DblClick;
end;

end.


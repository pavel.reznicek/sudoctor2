unit TabletTest;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls,
  SudoctorTablet;

type

  { TTabletTestForm }

  TTabletTestForm = class(TForm)
    tbEnteringMode: TToggleBox;
    procedure FormCreate(Sender: TObject);
    procedure tbEnteringModeChange(Sender: TObject);
  private
    FTablet: TSudoctorTablet; // the testing tablet
  public
  end;

var
  TabletTestForm: TTabletTestForm;

implementation

{$R *.lfm}

{ TTabletTestForm }

// If a new form is born
procedure TTabletTestForm.FormCreate(Sender: TObject);
begin
  // Create the testing tablet and become its parent
  FTablet:=TSudoctorTablet.Create(Self);
  FTablet.Parent := Self;
  ClientHeight:=FTablet.Height;
end;

procedure TTabletTestForm.tbEnteringModeChange(Sender: TObject);
begin
  FTablet.EnteringMode:=tbEnteringMode.Checked;
end;

end.


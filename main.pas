unit Main;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, Menus,
  SudoctorTable;

type

  { TMainForm }

  TMainForm = class(TForm)
    MainMenu: TMainMenu;
    miNewGame: TMenuItem;
    miQuit: TMenuItem;
    miEnteringMode: TMenuItem;
    miGame: TMenuItem;
    procedure FormCloseQuery(Sender: TObject; var CanClose: boolean);
    procedure FormCreate(Sender: TObject);
    procedure miEnteringModeClick(Sender: TObject);
    procedure miNewGameClick(Sender: TObject);
    procedure miQuitClick(Sender: TObject);
  private
  public
    Table: TSudoctorTable;
  end;

var
  MainForm: TMainForm;

implementation

{$R *.lfm}

{ TMainForm }

procedure TMainForm.FormCreate(Sender: TObject);
begin
  // Give birth to a sudoctor table
  Table := TSudoctorTable.Create(Self);
  // Make this form its parent
  Table.Parent := Self;
  // Adjust the size
  AutoSize:=True;
end;

procedure TMainForm.FormCloseQuery(Sender: TObject; var CanClose: boolean);
var
  Response: TModalResult;
begin
  // Ask a question
  Response:=MessageDlg(
    'Do you really want to quit?',
    TMsgDlgType.mtWarning,
    mbYesNo,
    0,
    mbNo
  );
  // If the response is yes then allow to close
  if Response = mrYes then
    CanClose:=True
  else
    CanClose:=False;
end;

procedure TMainForm.miEnteringModeClick(Sender: TObject);
begin
  Table.EnteringMode:=miEnteringMode.Checked;
end;

procedure TMainForm.miNewGameClick(Sender: TObject);
begin
  Table.NewGame;
  miEnteringMode.Checked:=True;
end;

procedure TMainForm.miQuitClick(Sender: TObject);
begin
  Close;
end;

end.


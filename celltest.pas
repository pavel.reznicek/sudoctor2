unit CellTest;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, SudoctorCell;

type

  { TCellTestForm }

  TCellTestForm = class(TForm)
    tbInputCell: TToggleBox;
    procedure tbInputCellChange(Sender: TObject);
  private
    FCell: TSudoctorCell;
  public
    constructor Create(TheOwner: TComponent); override;
  end;

var
  CellTestForm: TCellTestForm;

implementation

{$R *.lfm}

{ TCellTestForm }

procedure TCellTestForm.tbInputCellChange(Sender: TObject);
begin
  FCell.IsInputCell:=tbInputCell.Checked;
  if tbInputCell.Checked then
    tbInputCell.Caption:='&Input Cell'
  else
    tbInputCell.Caption:='S&olving Cell';
end;

constructor TCellTestForm.Create(TheOwner: TComponent);
begin
  inherited Create(TheOwner);
  FCell:=TSudoctorCell.Create(Self);
  FCell.Parent := Self;
end;

end.


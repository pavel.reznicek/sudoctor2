unit SudoctorDimensions;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

const
  // Tablet side
  TabletSide = 3;

  // Min. & max. sign numbers
  NoSignNumber = 0;
  MinSignNumber = 1;
  MaxSignNumber = sqr(TabletSide);

  // Cell index bounds
  MinCellIndex = Pred(MinSignNumber);
  MaxCellIndex = Pred(MaxSignNumber);

  // Cell-in-tablet index bounds
  MaxHorzCellInTabletIndex = Pred(TabletSide);
  MaxVertCellInTabletIndex = Pred(TabletSide);

  // Tablet dimensions
  TabletWidth = TabletSide;
  TabletHeight = TabletSide;

  // Table dimensions
  TableWidth = MaxSignNumber;
  TableHeight = MaxSignNumber;

  // Table dimensions in tablets
  HorzTabletCountInTable = TableWidth div TabletWidth;
  VertTabletCountInTable = TableHeight div TabletHeight;

  // Tablet index bounds
  MaxHorzTabletIndex = Pred(HorzTabletCountInTable);
  MaxVertTabletIndex = Pred(VertTabletCountInTable);

type
  // Possible sign number range (including 0 for no digit)
  TSignNumber = MinSignNumber - 1 .. MaxSignNumber; // the type used
  // Valid    sign number range (excluding 0 for no digit)
  TValidSignNumber = MinSignNumber .. MaxSignNumber; // range checking
  // Cell index range
  TSudoctorCellIndex = MinCellIndex .. MaxCellIndex;
  // The index type for the tablet list - copies the cell index type
  TSudoctorTabletIndex = type TSudoctorCellIndex;
  // The index type for the compl. sp. list - copies the cell index type
  TSudoctorCSpaceIndex = type TSudoctorCellIndex;

implementation

end.

